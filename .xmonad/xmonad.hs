-- Put the following in your .vimrc to have folds enabled by default:
-- `au BufEnter *.hs set foldmethod=marker | set foldmarker={{{,}}}` | norm zM

-- Imports {{{
-- Main Imports {{{
import           XMonad
import           XMonad.Layout
import           XMonad.Operations
import           XMonad.Config.Desktop
import           XMonad.Hooks.DynamicLog (dynamicLogWithPP, defaultPP, xmobarPP, wrap, pad, xmobarColor, shorten, PP(..))
import           XMonad.ManageHook
import qualified XMonad.StackSet               as W
-- }}}

-- Additional Imports {{{
import           System.Exit
import           Data.Monoid
import           Data.Maybe (isJust)
import qualified Data.Map                      as M
import           Data.List
import           Graphics.X11.ExtraTypes.XF86
import           Control.Monad
-- }}}

-- Actions {{{
import           XMonad.Actions.CycleWS
import           XMonad.Actions.Minimize
import           XMonad.Actions.CopyWindow
import qualified XMonad.Actions.FlexibleResize as Flex
import           XMonad.Actions.WindowBringer
import           XMonad.Actions.WithAll
-- }}}

-- Hooks {{{
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.ManageHelpers
import           XMonad.Hooks.EwmhDesktops
import           XMonad.Hooks.SetWMName
import           XMonad.Hooks.Place
import           XMonad.Hooks.PerWindowKbdLayout -- xmonad-extras
-- }}}

-- Util {{{
import           XMonad.Util.EZConfig
import           XMonad.Util.NamedScratchpad
import           XMonad.Util.Run (spawnPipe, hPutStrLn)
import           XMonad.Util.NoTaskbar
-- }}}

-- Layouts {{{
import           XMonad.Layout.ResizableTile
import           XMonad.Layout.Tabbed
import           XMonad.Layout.ZoomRow
import           XMonad.Layout.TwoPane
-- }}}

-- Layout modifiers {{{
import           XMonad.Layout.Minimize
import           XMonad.Layout.NoBorders
import           XMonad.Layout.BoringWindows
import qualified XMonad.Layout.ToggleLayouts as TL
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.MultiToggle.Instances
import           XMonad.Layout.Spacing
import qualified XMonad.Layout.Renamed as R
import           XMonad.Layout.WindowArranger
import           XMonad.Layout.ComboP
import qualified XMonad.Layout.WindowNavigation as WN
import           XMonad.Layout.TabBarDecoration
import           XMonad.Layout.Gaps
-- }}}
-- }}}

-- Definitions {{{
-- Terminal command to use
myTerm = "alacritty"
-- Window Count
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- Get the name of the active layout.
getActiveLayoutDescription :: X String
getActiveLayoutDescription = do
    workspaces <- gets windowset
    return $ description . W.layout . W.workspace . W.current $ workspaces
-- }}}

-- The config itself {{{
main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar ~/.xmonad/bar/xmobarrc"
  xmonad $ docks $ ewmh desktopConfig
    { manageHook            = myManageHook
    , terminal              = myTerm
    , modMask               = mod4Mask
    , keys                  = myRedefinedKeys
    , focusedBorderColor    = "#458588"
    , normalBorderColor     = "#282828"
    , borderWidth           = 2
    , layoutHook            = myLayout
    , handleEventHook       = perWindowKbdLayout
    , logHook               = dynamicLogWithPP . namedScratchpadFilterOutWorkspacePP
                            $ xmobarPP
                                  { ppOutput  = \x -> hPutStrLn xmproc x
                                  , ppCurrent = xmobarColor "lightblue" "" . wrap "[ " " ]"
                                  , ppTitle   = xmobarColor "lightblue" "" . wrap "[ " " ]" . shorten 110
                                  , ppSep     = "<fc=green> :: </fc>"
                                  , ppExtras  = [windowCount]
                                  , ppLayout  = xmobarColor "lightblue" "" . wrap "[ " " ]"
                                  , ppOrder   = \(ws:l:t:ex) -> [ws,l]++ex++[t]
                                  }
    , startupHook           = myStartupHook
    } `additionalKeysP` myAdditionalKeys `additionalMouseBindings` myMouseBinds
-- }}}

-- Startup {{{
myStartupHook = do
  setWMName "LG3D"
  spawn "sleep 1; \
        \ trayer-srg \
        \ --SetDockType true \
        \ --SetPartialStrut false \
        \ --edge top \
        \ --align right \
        \ --expand true \
        \ --width 5 \
        \ --height 18 \
        \ --transparent true \
        \ --tint 0x282828 \
        \ --alpha 20 \
        \ --distancefrom right \
        \ --distance 10"
-- }}}

-- Layouts {{{
myLayout =
        smartBorders
        $ windowArrange
        $ R.renamed [R.CutWordsLeft 1]
        $ boringWindows
        $ minimize
        $ avoidStrutsOn [U]
        $ mkToggle (NBFULL ?? EOT)
        $ mkToggle (single MIRROR)
        $ TL.toggleLayouts tabext (tiled) ||| tab
    where
        tiled  = R.renamed [R.Replace "Tall"]
               $ spacingRaw True (Border 5 5 5 5) True (Border 5 5 5 5) True
               $ ResizableTall 1 (3/100) (1/2) []

        tab    = R.renamed [R.Replace "Tab"]
               $ tabbed shrinkText myTabTheme

        tabext = R.renamed [R.Replace "TabExt"]
               $ smartBorders
               $ WN.windowNavigation (combineTwoP (TwoPane 0.03 0.75)
                                      (noBorders tab)
                                      ( tabBar shrinkText myTabTheme Top
                                      $ gaps [(U,20)]
                                      $ Mirror zoomRow)
                                      myProp
                                     )

-- Define windows to put in main pane in TabExt {{{
myProp :: Property
myProp = (Or
          (Or elseProp editorProp)
          (Or officeProp mediaProp)
         )

mediaProp  = (Or (ClassName "Sxiv")    (ClassName "mpv"))
officeProp = (Or (ClassName "Wine")    (Resource  "libreoffice"))
elseProp   = (Or (ClassName "Zathura") (ClassName "Firefox"))
editorProp = (Or (ClassName "NVim")    (ClassName "Emacs"))
-- }}}

-- Theme for tabbed layout {{{
myTabTheme = def { fontName            = "xft:sans:size=10:antialias=true"
                 , decoHeight          = 20
                 , activeColor         = "#282828"
                 , inactiveColor       = "#282828"
                 , activeBorderColor   = "#458588"
                 , inactiveBorderColor = "#282828"
                 , activeTextColor     = "lightblue"
                 }
-- }}}
-- }}}

-- ManageHook {{{
myManageHook = placeHook myPlacement <+> windowsManageHook <+> scratchHook <+> def

-- Place floating windows in center
myPlacement = withGaps (16,0,16,0) (smart (0.5,0.5))

-- Window hooks {{{
windowsManageHook = composeOne
                  [ (className =? "FireFox" <&&> resource =? "Dialog") -?> doFloat
                  , className  =? "Xfe" -?> doFloat
                  , resource   =? "maim" -?> doIgnore
                  , resource   =? "stalonetray" -?> doIgnore
                  , className  =? "trayer" -?> doIgnore
                  , className  =? "Slop" -?> doIgnore
                  , return True -?> doF W.swapDown
                  , isDialog -?> doF W.shiftMaster <+> doF W.swapDown
                  ]
-- }}}

-- Scratchpad hook {{{
scratchHook = namedScratchpadManageHook scratchpads

scratchpads = [
              -- Run tmux scratch session in default floating window
              NS "scratch" spawnTerm findTerm manageTerm
              -- , NS "tray" spawnTray findTray manageTray
              ]
              where
                spawnTerm  = "st -c scratch -e tmuxdd"
                findTerm   = className =? "scratch"
                manageTerm = customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3)
                spawnTray  = "stalonetray"
                findTray   = className =? "stalonetray"
                manageTray = customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3)
-- }}}
-- }}}

-- Additional Keybindings {{{
myAdditionalKeys =
        [
        -- Launchers {{{
          ("M-<Return>", spawn myTerm)
        , ("M-d w",      spawn "firefox")
        , ("M-d S-w",    spawn (myTerm ++ " -e sudo nmtui"))
        , ("M-d r",      spawn (myTerm ++ " -e vu"))
        , ("M-d S-r",    spawn "xfe")
        , ("M-d e",      spawn (myTerm ++ " -e vk"))
        , ("M-d C-e",    spawn "telegram-desktop")
        , ("M-c",        spawn "clipmenu")
        , ("M-d i",      spawn (myTerm ++ " -e htop"))
        , ("M-a",        spawn "xkb-switch -n")
        , ("M-d a",      spawn (myTerm ++ " -e pulsemixer"))
        -- }}}
        -- Dmenu scripts {{{
        , ("M-d d",      spawn "exe=`dmenu_path | dmenu` && eval \"exec $exe\"")
        , ("M-d C-a",    spawn "dmenuopener")
        , ("M-`",        spawn "dmenuunicode")
        , ("<Print>",    spawn "screenshot")
        , ("S-<Print>",  spawn "scrotpick")
        , ("M-<Print>",  spawn "dmenurecord")
        , ("M-<Delete>", spawn "killrecording")
        , ("M-<F3>",     spawn "arandr")
        , ("M-<F4>",     spawn "prompt 'Suspend?' 'sudo s2ram'")
        , ("M-<F5>",     spawn "sudo rc-service NetworkManager restart")
        , ("M-<F6>",     spawn "st -e torwrap")
        , ("M-<F7>",     spawn "td-toggle")
        , ("M-<F8>",     spawn "")
        , ("M-<F9>",     spawn "dmenumount")
        , ("M-<F10>",    spawn "dmenuumount")
        , ("M-<F11>",    spawn "ducksearch")
        , ("M-<F12>",    spawn "prompt 'Hibernate?' 'sudo hibernate'")
        -- }}}
        -- Controls {{{
        -- , ("M-=" , spawn "lmc up 5")
        -- , ("M--" , spawn "lmc down 5")
        , ("<XF86AudioRaiseVolume>",  spawn "amixer -q sset Master 5%+")
        , ("<XF86AudioLowerVolume>",  spawn "amixer -q sset Master 5%-")
        , ("<XF86AudioMute>",         spawn "amixer -q sset Master toggle")
        -- , ("<XF86MonBrightnessUp>",   spawn "xbacklight -inc 10")
        -- , ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 10")
        , ("M-S-x",                   spawn "prompt 'Shutdown?' 'sudo poweroff'")
        , ("M-C-x",                   spawn "prompt 'Reboot?' 'sudo reboot'")
        -- }}}
        -- Window management {{{
        , ("M-[",                          sendMessage zoomIn)
        , ("M-]",                          sendMessage zoomOut)
        , ("M-'",                          sendMessage ZoomFullToggle)
        , ("M-x",                          twoLayouts (
                                              ("TabExt", (sendMessage $ SwapWindow))
                                            , ("Tall",   (windows W.swapMaster))
                                            )
          )
        , ("M-h",                          sendMessage $ WN.Go WN.L)
        , ("M-l",                          sendMessage $ WN.Go WN.R)
        , ("M-C-<Return>",                 windows W.swapMaster)
        , ("M-<Space>",                    sendMessage NextLayout)
        -- , ("M-S-<Space>" , setLayout $ XMonad.layoutHook conf)
        , ("M-b",                          sendMessage ToggleStruts)
        , ("M-C-,",                        sendMessage (IncMasterN 1))
        , ("M-C-.",                        sendMessage (IncMasterN (-1)))
        , ("M-C-h",                        sendMessage Shrink)
        , ("M-C-l",                        sendMessage Expand)
        {-- , ("M-j" , windows W.focusDown)
        -- , ("M-j" , windows W.focusDown)
        -- , ("M-m" , windows W.focusMaster) --}
        , ("M-j" , do
          layout <- getActiveLayoutDescription
          case layout of
            "Tall" -> focusDown
            "Tab" -> focusDown
            "TabExt" -> sendMessage $ WN.Go WN.D
          )
        , ("M-k" , do
          layout <- getActiveLayoutDescription
          case layout of
            "Tall" -> focusUp
            "Tab" -> focusUp
            "TabExt" -> sendMessage $ WN.Go WN.U
          )
        , ("M-m" , do
          layout <- getActiveLayoutDescription
          case layout of
            "TabExt" -> focusDown
          )
        , ("M-," , do
          layout <- getActiveLayoutDescription
          case layout of
            "TabExt" -> focusUp
          )
        , ("M-<Backspace>", focusMaster)
        , ("M-S-h",         sendMessage MirrorShrink)
        , ("M-S-l",         sendMessage MirrorExpand)
        , ("M-C-j",         windows W.swapDown)
        , ("M-C-k",         windows W.swapUp)
        , ("M-n",           withFocused minimizeWindow)
        , ("M-S-n",         withFirstMinimized maximizeWindowAndFocus)
        , ("M-C-n",         withLastMinimized maximizeWindowAndFocus)
        , ("M-q",           kill1)
        , ("M-S-q",         windows copyToAll)
        , ("M-C-q",         killAllOtherCopies)
        , ("M-S-r",         refresh)
        , ("M-y",           moveTo Prev nonEmptyNonNSP)
        , ("M-o",           moveTo Next nonEmptyNonNSP)
        , ("M-C-y",         moveTo Prev nonNSP)
        , ("M-C-o",         moveTo Next nonNSP)
        , ("M-i",           shiftToPrev >> prevWS)
        , ("M-u",           shiftToNext >> nextWS)
        , ("M-C-i",         shiftTo Prev nonEmptyNonNSP >> moveTo Prev nonEmptyNonNSP)
        , ("M-C-u",         shiftTo Next nonEmptyNonNSP >> moveTo Next nonEmptyNonNSP)
        -- , ("M-f" , sequence_
        --             [sendMessage $ Toggle NBFULL, sendMessage $ ToggleStrut U])
        , ("M-f",           sendMessage (Toggle NBFULL) >> sendMessage (ToggleStrut U))
        , ("M-C-b",         sendMessage TL.ToggleLayout)
        , ("M-C-f",         withFocused float)
        , ("M-t",           withFocused $ windows . W.sink)
        , ("M-C-t",         sinkAll)
        , ("M-C-m",         sendMessage $ Toggle MIRROR)
        , ("M-s",           namedScratchpadAction scratchpads "scratch")
        , ("M-r",           sendMessage Arrange)
        , ("M-C-r",         sendMessage DeArrange)
        , ("M-S-y",         sendMessage (MoveLeft 20))
        , ("M-S-o",         sendMessage (MoveRight 20))
        , ("M-S-u",         sendMessage (MoveDown 20))
        , ("M-S-i",         sendMessage (MoveUp 20))
        , ("M--",           sendMessage (IncreaseLeft 20))
        , ("M-=",           sendMessage (IncreaseRight 20))
        , ("M-S--",         sendMessage (IncreaseDown 20))
        , ("M-S-=",         sendMessage (IncreaseUp 20))
        , ("M-C--",         sendMessage (DecreaseRight 20))
        , ("M-C-=",         sendMessage (DecreaseLeft 20))
        , ("M-C-S--",       sendMessage (DecreaseUp 20))
        , ("M-C-S-=",       sendMessage (DecreaseDown 20))
        , ("M-w g",         gotoMenu)
        , ("M-w b",         bringMenu)
        -- }}}
        -- Restart
        , ("M-<F1>",        spawn "xmonad --recompile; xmonad --restart")
        -- Exit
        , ("M-<Esc>",       io (exitWith ExitSuccess))
        ] where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
-- }}}

-- Map key per layout {{{
  {--
        , ("keys" , do
          layout <- getActiveLayoutDescription
          case layout of
            "layoutname" -> ...
          )
  --}
ifLayout :: (String, X ()) -> X ()
ifLayout a = do
  layout <- getActiveLayoutDescription
  when (fst a == layout) (snd a)

twoLayouts :: ( (String , X ()) , (String , X ()) ) -> X ()
twoLayouts a = do
  ifLayout (fst a)
  ifLayout (snd a)
-- }}}

-- Mouse bindings {{{
myMouseBinds =
  [
  ( (mod4Mask, button3) , (\w -> focus w >> Flex.mouseResizeWindow w) )
  ]
-- }}}

-- XMonad keys, leave only keys for monitors and workspaces {{{
myRedefinedKeys conf@(XConfig { XMonad.modMask = modm }) =
        M.fromList
                $
    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-control-{w,e,r}, Move client to screen 1, 2, or 3
    --
                   [ ( (m .|. modm, key)
                   , screenWorkspace sc >>= flip whenJust (windows . f)
                   )
                   | (key, sc) <- zip [xK_bracketleft, xK_bracketright, xK_backslash] [0 ..]
                   , (f  , m ) <- [(W.view, 0), (W.shift, controlMask)]
                   ]
                ++
-- mod-[1..9] @@ Switch to workspace N
-- mod-shift-[1..9] @@ Move client to workspace N
-- mod-shift-[1..9] @@ Copy client to workspace N
                   [((m .|. modm, k), windows $ f i)
                       | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
                       , (f, m) <- [(W.view, 0), (W.shift, controlMask), (copy, shiftMask)]]
-- }}}
